
Compile and Run Notes

	This project requires JDK 1.8 to compile and run.
	
	This project can be imported into Eclipse as a maven project (or as an 
	Eclipse project if the .classpath, .project, and .settings/ files/directory
	are there).
	
	It can be run from within Eclipse, or from the command line with
	
		mvn clean package
	    java -jar target/producer.consumer.example-0.0.1-SNAPSHOT-jar-with-dependencies.jar
	 
	
	Logging is written to the console and to the producer.consumer.log file.
	
	The WidgetStatus results are written to the producer.consumer.status.report.TIMESTAMP.log file.


Design Notes

	Java 8 CompletableFutures and ExecutorServices are used to implement multiple threads.
	
	ProducerMaster and ConsumerMaster classes run on their own threads and create Producers and
	Consumers that run on their own threads.  
	
	This application uses 10 threads: main, producermaster, consumermaster,
	5 producers, and 2 consumers.
	
	Producers and Consumers use a read-only singleton class Completion.java 
	to determine when it is time to shutdown.

 	The JUnit test runs the Producer and Consumer in serial to verify correct operation of each.
 