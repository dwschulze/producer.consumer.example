package test;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import org.junit.Test;

import example.prod.cons.main.Completion;
import example.prod.cons.masters.ConsumerMaster;
import example.prod.cons.masters.ProducerMaster;
import example.prod.cons.model.Widget;
import example.prod.cons.model.WidgetStatus;

public class ProducerMasterConsumerMasterTest {

	long RUNTIME_MS = 5000;
	int NUM_PRODUCERS = 5;
	double WIDGETS_PER_SECOND = 1.0;
	int NUM_CONSUMERS = 2;
	int CONSUMER_PROCESSING_TIME_MIN = 250;
	int CONSUMER_PROCESSING_TIME_MAX = 1250;
	int MAX_CAPACITY = 10000;
	
	@Test
	public void testProducerMasterConsumerMaster() {
		
		
		long capacity = (long)(NUM_PRODUCERS * WIDGETS_PER_SECOND * RUNTIME_MS / 1000.0);
		long queueShouldContain = capacity;
		
		if (capacity > MAX_CAPACITY) {
			System.out.println("Reducing capacity from " + capacity + " to " + MAX_CAPACITY);
			capacity = MAX_CAPACITY;
		} else {
			System.out.println("Queue capacity = " + capacity);
		}

		ArrayBlockingQueue<Widget> widgetQueue = new ArrayBlockingQueue<Widget>((int)capacity, true);
		
		long startTimeMs = (new Date()).getTime();
		Completion.instance().initialize(startTimeMs, RUNTIME_MS, widgetQueue);
		
		WidgetStatus status = new WidgetStatus();
		ProducerMaster pm = new ProducerMaster(NUM_PRODUCERS, WIDGETS_PER_SECOND, widgetQueue, status);
		pm.run();
		long stopTimeMs = (new Date()).getTime();

		assertTrue("Producer did not complete.", Completion.instance().isProducerCompleted());
		
		long runtime = stopTimeMs - startTimeMs;
		long upperLimit = (int)(1.05*RUNTIME_MS);
		
		assertTrue("Runtime = " + runtime + " was less than "  + RUNTIME_MS, runtime >= RUNTIME_MS);
		assertTrue("Runtime = " + runtime + " was too much greater than upperLimit = "  + upperLimit, runtime < upperLimit);

		int sz = widgetQueue.size();
		
		assertTrue("widgetQueue size = " + sz + " not correct size of " + queueShouldContain, sz == queueShouldContain);

		System.out.println("ProducerMasterTest complete.");
		
		ConsumerMaster cm = new ConsumerMaster(NUM_CONSUMERS, widgetQueue, status, CONSUMER_PROCESSING_TIME_MIN, CONSUMER_PROCESSING_TIME_MAX);
		
		long consumerMasterStartTimeMs = (new Date()).getTime();
		cm.run();
		long consumerMasterEndTimeMs = (new Date()).getTime();
		
		assertTrue("widgetQueue is not empty after ConsumerMasterTest.", widgetQueue.size()==0);

		long consumerMasterRuntimeMs = consumerMasterEndTimeMs - consumerMasterStartTimeMs;
		int minMs = (sz*CONSUMER_PROCESSING_TIME_MIN)/NUM_CONSUMERS;
		int maxMs = (sz*CONSUMER_PROCESSING_TIME_MAX)/NUM_CONSUMERS;

		assertTrue("ConsumerMaster runtime out of range: " + minMs + "-" + maxMs, 
			((minMs <= consumerMasterRuntimeMs) && (consumerMasterRuntimeMs <= maxMs)));

		System.out.println("ConsumerMasterTest complete.");
		
	}
}
