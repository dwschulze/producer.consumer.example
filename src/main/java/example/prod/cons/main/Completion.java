package example.prod.cons.main;

import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import example.prod.cons.model.Widget;

public class Completion {

	long startTimeInMilliseconds;
	long runtimeInMilliseconds;
	ArrayBlockingQueue<Widget> widgetQueue;
	boolean initialized = false;
	private boolean producerCompleted = false;
	
	//	instance_ could be initialized in a static initializer block
	//	to avoid the null check in the instance() method.
	private static Completion instance_ = null;
	
	private Completion() {}
	
	public static synchronized Completion instance() {
		
		if (null == instance_)
			instance_ = new Completion();
		
		return instance_;
	}
	
	public void initialize(
		long startTimeInMilliseconds_,
		long runtimeInMilliseconds_, 
		ArrayBlockingQueue<Widget> widgetQueue_) {

		if (initialized)
			throw new RuntimeException("Can only call initialize() once.");
		
		startTimeInMilliseconds = startTimeInMilliseconds_;
		runtimeInMilliseconds = runtimeInMilliseconds_;
		widgetQueue = widgetQueue_;
		initialized = true;
	}


	public void setProducerCompleted(boolean b) {
		producerCompleted = b;
	}
	
	public boolean isProducerCompletionCriteriaMet() {
	
		if (!initialized)
			throw new RuntimeException("Not initialized");
		
		long nowInMilliseconds = (new Date()).getTime();
		
		if ((nowInMilliseconds - startTimeInMilliseconds) > runtimeInMilliseconds)
			return true;
		
		return false;
	}
	
	public boolean isConsumerCompletionCriteriaMet() {
		
		if (!isProducerCompletionCriteriaMet())
			return false;
		
		if (!producerCompleted)
			return false;
		
		if (!widgetQueue.isEmpty())
			return false;
		
		return true;
	}
	
	public boolean isProducerCompleted() {
		
		return producerCompleted;
	}
}
