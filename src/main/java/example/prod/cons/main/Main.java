package example.prod.cons.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import example.prod.cons.masters.ConsumerMaster;
import example.prod.cons.masters.ProducerMaster;
import example.prod.cons.model.Widget;
import example.prod.cons.model.WidgetStatus;

public class Main {

	private static Logger logger = Logger.getLogger(Main.class);
	static final long RUNTIME_MS = 50000;
	static final int NUM_PRODUCERS = 5;
	static final double WIDGETS_PER_SECOND = 1.0;
	static final int NUM_CONSUMERS = 2;
	static final int CONSUMER_PROCESSING_TIME_MIN = 250;
	static final int CONSUMER_PROCESSING_TIME_MAX = 1250;
	static final int MAX_CAPACITY = 10000;
	
	public static void main(String[] args) {

		long capacity = (long)(NUM_PRODUCERS * WIDGETS_PER_SECOND * RUNTIME_MS / 1000.0);
		if (capacity > MAX_CAPACITY) {
			logger.warn("Reducing capacity from " + capacity + " to " + MAX_CAPACITY);
			capacity = MAX_CAPACITY;
		} else {
			logger.info("Queue capacity = " + capacity);
		}
			
		ArrayBlockingQueue<Widget> widgetQueue = new ArrayBlockingQueue<Widget>((int)capacity, true);
		WidgetStatus status = new WidgetStatus();
		
		Completion.instance().initialize((new Date()).getTime(), RUNTIME_MS, widgetQueue);
		
		long startTime = (new Date()).getTime();
		
		ExecutorService producerMasterExecutor = Executors.newFixedThreadPool(1);
		CompletableFuture.supplyAsync( () -> { 
			
			ProducerMaster pm = new ProducerMaster(NUM_PRODUCERS, WIDGETS_PER_SECOND, widgetQueue, status);
			pm.run();
			return NUM_PRODUCERS + " producers completed.";
			
		}, producerMasterExecutor).whenComplete( (result, err) -> {

			if (null != err)
				logger.error("Throwable:  ", err);
			else
				logger.info(result);
		});

		ExecutorService consumerMasterExecutor = Executors.newFixedThreadPool(1);
		CompletableFuture.supplyAsync( () -> { 
			
			ConsumerMaster cm = new ConsumerMaster(NUM_CONSUMERS, widgetQueue, status, CONSUMER_PROCESSING_TIME_MIN, CONSUMER_PROCESSING_TIME_MAX);
			cm.run();
			return NUM_CONSUMERS + " consumers completed.";
			
		}, consumerMasterExecutor).whenComplete( (result, err) -> {

			if (null != err)
				logger.error("Throwable:  ", err);
			else
				logger.info(result);
		});

		producerMasterExecutor.shutdown();
		try {
			producerMasterExecutor.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
			logger.info("producerMasterExecutor terminated.");
		} catch (InterruptedException e) {
			logger.warn(e);
		}


		consumerMasterExecutor.shutdown();
		try {
			consumerMasterExecutor.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
			logger.info("consumerMasterExecutor terminated.");
		} catch (InterruptedException e) {
			logger.warn(e);
		}

		long endTime = (new Date()).getTime();
		
		logger.info("Test runtime in Milliseconds = " + (endTime - startTime));

		String statusFileName = getStatusFileName();
		try {
			PrintStream ps = new PrintStream(new File(statusFileName));
			status.writeToFile(ps);
		} catch (FileNotFoundException e) {
			logger.error(e);
		}
	}

	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss.SSS");
	private static String getStatusFileName() {
		
		Date now = new Date();
		return "producer.consumer.status.report." + sdf.format(now) + ".txt";
	}

}
