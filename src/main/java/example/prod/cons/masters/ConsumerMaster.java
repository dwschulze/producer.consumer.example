package example.prod.cons.masters;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import example.prod.cons.main.Completion;
import example.prod.cons.model.Widget;
import example.prod.cons.model.WidgetStatus;

public class ConsumerMaster {

	private static Logger logger = Logger.getLogger(ConsumerMaster.class);
	
	int numConsumers;
	ArrayBlockingQueue<Widget> queue;
	WidgetStatus status;
	int processingTimeMin;
	int processingTimeRange;
	Random rand = new Random();
	
	
	public ConsumerMaster(
		int numConsumers_, 
		ArrayBlockingQueue<Widget> queue_,
		WidgetStatus status_, 
		int processingTimeMin_,
		int processingTimeMax_) {
		
		numConsumers = numConsumers_;
		queue = queue_;
		status = status_;
		
		if (processingTimeMax_ <= processingTimeMin_)
			throw new RuntimeException("processingTimeMax must be > processingTimeMin:  processingTimeMin=" + processingTimeMin_ + ", processingTimeMax_=" + processingTimeMax_);
	
		processingTimeMin = processingTimeMin_;
		
		//	Add 1 to make the range inclusive
		processingTimeRange = processingTimeMax_ - processingTimeMin_ + 1;
	}
	
	public void run() {
		
		ExecutorService consumerExecutor = Executors.newFixedThreadPool(numConsumers);
		
		logger.info("Launching " + numConsumers + " consumers.");
		
		for (int i=0; i<numConsumers; i++) {

			final int consumerNum = i;
			CompletableFuture.supplyAsync( () -> { 
				
				logger.info("Launching consumer " + consumerNum);
				
				Consumer consumer = new Consumer();
				consumer.run();
				
				return consumerNum;
				
			}, consumerExecutor).whenComplete( (result, err) -> {
	
				if (null != err)
					logger.error("Throwable:  ", err);
				else
					logger.info("Consumer " + result + " completed successfully.");
			});
		}

		consumerExecutor.shutdown();
		try {
			consumerExecutor.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
			logger.info("consumerExecutor terminated.");
		} catch (InterruptedException e) {
			logger.warn(e);
		}
	}
	
	
	class Consumer {
		
		public void run() {
		
			String threadId = Thread.currentThread().getName();

			while (!Completion.instance().isConsumerCompletionCriteriaMet()) {

				Widget w = queue.poll();
				
				if (null != w) 
					status.incrementWidgetCountForConsumerId(threadId);
				
				long sleepTimeInMilliseconds = processingTimeMin + rand.nextInt(processingTimeRange);
				try {
					Thread.sleep(sleepTimeInMilliseconds);
				} catch (InterruptedException e) {
					logger.error(e);
				}
			}
			
			status.setThreadIdStopDate(threadId, new Date());
		}
	}	//	class Consumer

}
