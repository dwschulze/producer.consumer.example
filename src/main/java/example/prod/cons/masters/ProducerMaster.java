package example.prod.cons.masters;

import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import example.prod.cons.main.Completion;
import example.prod.cons.model.Widget;
import example.prod.cons.model.WidgetStatus;


public class ProducerMaster {

	private static Logger producerMasterLogger = Logger.getLogger(ProducerMaster.class);
	
	int numProducers;
	double productionFrequency;
	ArrayBlockingQueue<Widget> queue;
	WidgetStatus status;
	int sleepMs;
	
	
	public ProducerMaster(
		int numProducers_, 
		double productionFrequency_, 
		ArrayBlockingQueue<Widget> queue_, 
		WidgetStatus status_) {
		
		numProducers = numProducers_;
		productionFrequency = productionFrequency_;
		queue = queue_;
		status = status_;
		sleepMs = (int)(1000.0 / productionFrequency);
	}

	public void run() {
		
		ExecutorService producerExecutor = Executors.newFixedThreadPool(numProducers);
		
		producerMasterLogger.info("Launching " + numProducers + " producers.");
		for (int i=0; i<numProducers; i++) {
			
			final int producerNum = i;
			CompletableFuture.supplyAsync( () -> { 
				
				producerMasterLogger.info("Launching producer " + producerNum);
				
				Producer producer = new Producer();
				producer.run();
				
				return producerNum;
				
			}, producerExecutor).whenComplete( (result, err) -> {
				
				if (null != err)
					producerMasterLogger.error("Throwable:  ", err);
				else
					producerMasterLogger.info("Producer " + result + " completed successfully.");
			});
		}

		producerExecutor.shutdown();
		try {
			producerExecutor.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
			Completion.instance().setProducerCompleted(true);
			producerMasterLogger.info("producerExecutor terminated.");
		} catch (InterruptedException e) {
			producerMasterLogger.warn(e);
		}

	}
	
	class Producer {
		
		public void run() {
			
			String threadId = Thread.currentThread().getName();

			while (!Completion.instance().isProducerCompletionCriteriaMet()) {
			
				Widget w = new Widget();
				long lastProductionTime = (new Date()).getTime();
				status.incrementWidgetCountForProducerId(threadId);

				try {
					boolean taken = queue.offer(w, sleepMs, TimeUnit.MILLISECONDS);
					
					if (!taken)
						producerMasterLogger.error("queue.offer timed out on thread " + threadId);
					
				} catch (InterruptedException e) {
					producerMasterLogger.error(e);
				}
				
				//	Account for queueing / blocking time when sleeping
				long remainingSleepTime = sleepMs - ((new Date()).getTime() - lastProductionTime);
				try {
					Thread.sleep(remainingSleepTime);
				} catch (InterruptedException e) {
					producerMasterLogger.error(e);
				}
			}
			
			status.setThreadIdStopDate(threadId, new Date());
		}
	}
}
