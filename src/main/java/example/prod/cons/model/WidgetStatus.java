package example.prod.cons.model;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


//The Producer and Consumer also share a WidgetStatus object that as Widgets 
//are produced and consumed keeps track of:
//    The number of Widgets produced by each Producer
//    The total number of Widgets produced so far
//    The number of Widgets consumed by each Consumer
//    The total number of Widgets consumed so far
//    The time at which each thread stopped running

//	Generate a text file that lists the information in the WidgetStatus object
 
public class WidgetStatus {

	String newline = System.getProperty("line.separator");
	private Map<String, AtomicInteger> producerIdCountMap = new ConcurrentHashMap<String, AtomicInteger>();
	private Map<String, AtomicInteger> consumerIdCountMap = new ConcurrentHashMap<String, AtomicInteger>();
	AtomicLong widgetsProduced = new AtomicLong();
	AtomicLong widgetsConsumed = new AtomicLong();
	private Map<String, Date> threadIdStopDateMap  = new ConcurrentHashMap<String, Date>();
	
	public void incrementWidgetCountForProducerId(String producerId) {

		if (producerIdCountMap.containsKey(producerId))
			producerIdCountMap.get(producerId).incrementAndGet();
		else
			producerIdCountMap.put(producerId, new AtomicInteger(1));
		
		widgetsProduced.incrementAndGet();
	}
	
	
	public void incrementWidgetCountForConsumerId(String consumerId) {

		if (consumerIdCountMap.containsKey(consumerId))
			consumerIdCountMap.get(consumerId).incrementAndGet();
		else
			consumerIdCountMap.put(consumerId, new AtomicInteger(1));
		
		widgetsConsumed.incrementAndGet();
	}
	
	public void setThreadIdStopDate(String threadId, Date stopDate) {
		threadIdStopDateMap.put(threadId, stopDate);
	}
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss.SSS");

	public void writeToFile(PrintStream s) {

		s.printf("Widgets produced: %d" + newline, widgetsProduced.get());
		for (String threadId:producerIdCountMap.keySet())
			s.printf("    %s:  %d" + newline, threadId, producerIdCountMap.get(threadId).get());

		s.printf("Widgets consumed: %d" + newline, widgetsConsumed.get());
		for (String threadId:consumerIdCountMap.keySet())
			s.printf("    %s:  %d" + newline, threadId, consumerIdCountMap.get(threadId).get());
		
		s.printf("Thread stop times" + newline);
		List<String> l = new ArrayList<String>(threadIdStopDateMap.keySet());
		Collections.sort(l);
		for (String threadId : l)
			s.printf("    %s:  %s" + newline, threadId, sdf.format(threadIdStopDateMap.get(threadId)));
		
		s.close();
	}
}
